
// Alpha video
AFRAME.registerComponent(
    "alpha-video", {
        init: function() {
            this.materialLoaded = this.materialLoaded.bind(this);
            this.el.addEventListener("materialtextureloaded", this.materialLoaded);
        },
        materialLoaded: function() {
            let material = this.el.getObject3D("mesh").material;
            material.map.format = THREE.RGBAFormat;
            material.map.needsUpdate = true;
        },
        remove: function() {
            this.el.removeEventListener("materialtextureloaded", this.materialLoaded);
        }
    }
);

AFRAME.registerComponent("redirect-on-click", {
    init: function() {
        this.el.addEventListener("click", this.redirect);
        this.el.addEventListener("touchstart", this.redirect);
    },
    redirect: function() {
        window.location.href = "https://www.distrikt.ro/";
    },
    remove: function() {
        this.el.removeEventListener("click", this.redirect);
        this.el.removeEventListener("touchstart", this.redirect);
    }
})

AFRAME.registerComponent(
    "play-on-target-found", {
    init: function () {
        this.el.addEventListener("targetFound", this.targetFound);
        this.el.addEventListener("targetLost", this.targetLost);
    },
    targetFound: function () {
        const video = document.querySelector("#animation")
        video.pause();
        video.currentTime = 0;
        video.play();
    },
    targetLost: function () {
        const video = document.querySelector("#animation")
        video.pause();
    },
    remove: function () {
        this.el.removeEventListener("targetFound", this.targetFound);
        this.el.removeEventListener("targetLost", this.targetLost);
    }
})